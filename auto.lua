local S = minetest.get_translator()

local function check_filename(name)
	return name:find("^[%w%s%^&'@{}%[%],%$=!%-#%(%)%%%.%+~_]+$") ~= nil
end


local S = minetest.get_translator()

arena_lib.on_load("mesewars_2", function(arena)
	arena.teams[1].mese = true
	arena.teams[2].mese = true

	--	minetest.chat_send_all(tostring(arena.players["debiankaios"].teamID))
	local pos = arena.swpoint

	local path = minetest.get_worldpath() .. "/schems/" .. arena.schemname .. ".mts"
	if minetest.place_schematic(pos, path) == nil then
		minetest.chat_send_all(name, "failed to place Minetest schematic")
		--[[else
		worldedit.player_notify(name, "placed Minetest schematic " .. param ..
		" at " .. minetest.pos_to_string(pos))]]
	end
	mesewars.clear_objects(arena.swpoint, arena.nepoint)
	--minetest.chat_send_all(tostring(arena.teams[1].name))
	for p_name, stats in pairs(arena.players) do
		mesewars.p2.generate_HUD(arena, p_name)
	end
end)

arena_lib.on_join("mesewars_2", function(p_name, arena, as_spectator)
	mesewars.p2.generate_HUD(arena, p_name)
end)

arena_lib.on_quit("mesewars_2", function(arena, p_name, is_spectator, reason)
	mesewars.p2.remove_HUD(arena, p_name)
end)

arena_lib.on_death("mesewars_2", function(arena, p_name, reason)
	local player = minetest.get_player_by_name(p_name)
	local inv = player:get_inventory()
	if arena.teams[arena.players[p_name].teamID].mese == false then
		arena_lib.remove_player_from_arena(p_name, 1)
		for pl_name in pairs(arena.players_and_spectators) do
			minetest.chat_send_player(pl_name, S("Player @1 is eliminated", p_name))
		end
	else
		player:set_pos(arena_lib.get_random_spawner(arena, arena.players[p_name].teamID))
		inv:set_list("main", {})
		for pl_name in pairs(arena.players_and_spectators) do
			minetest.chat_send_player(pl_name, S("Player @1 died", p_name))
		end
	end
end)

arena_lib.on_celebration("mesewars_2", function(arena, winner_name)
	--[[for i in pairs(winner_name) do
		mesewars.add_mesecoins(winner_name[i], 100)
		minetest.chat_send_player(winner_name[i], "You recieved 100 mesecoins")
	end]]
	for p_name, stats in pairs(arena.players_and_spectators) do
    	mesewars.p2.remove_HUD(arena, p_name)
	end
    --player:hud_remove(blue)
    --player:hud_remove(orange)
    --player:hud_remove(background)
end)

--[[
orange = 1
blue = 2



arena_lib.on_start("mesewars", function(arena, player)
  team1 = arena.teams[1] --arena.players[p_name].teamID
end)
]]
