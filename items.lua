-- Own Nodes


	minetest.register_node("mesewars:wool_blue", {
			description = "Blue Wool",
			tiles = {"mesewars_wool_blue.png"},
			is_ground_content = false,
			groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3, wool = 1},

		})

	minetest.register_node("mesewars:wool_orange", {
  		description = "Orange Wool",
  		tiles = {"mesewars_wool_orange.png"},
  		is_ground_content = false,
  		groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3, wool = 1},
  	})

minetest.register_node("mesewars:wood", {
		description = "Mesewars Wood Planks",
		paramtype2 = "facedir",
		place_param2 = 0,
		tiles = {"default_wood.png"},
		is_ground_content = false,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, wood = 1},
	})

minetest.register_node("mesewars:stone", {
	description = "Stone",
	tiles = {"default_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = "mesewars:stone",
})

-- Mese

minetest.register_node("mesewars:mese_blue", {
	description = "Blues Mese",
	groups = {cracky=1, mw_cracky = 1},
	tiles = {"mesewars_blue_mese.png"},
	drop = "",
	on_dig = function(pos, node, player)
    	local p_name = player:get_player_name()
    	if arena_lib.is_player_in_arena(p_name, "mesewars_2") and not(arena_lib.is_player_spectating(p_name)) then
			local arena = arena_lib.get_arena_by_player(p_name)
			if arena.players[p_name].teamID == 2 then
        		minetest.chat_send_all("You can't dig your own mese")
			else
				arena.teams[2].mese = false
				minetest.node_dig(pos, node, player)
				mesewars.p2.update_mese_HUD(arena, 2)
    			for pl_name, stats in pairs(arena.players) do
            		minetest.chat_send_player(pl_name ,"Blues Mese was dug by " .. p_name)
    			end
				mesewars.add_mesecoins(p_name, 20)
				minetest.chat_send_player(p_name, "You recieved 20 mesecoins")
			end
		else
			minetest.node_dig(pos, node, player)
    	end
	end
})



minetest.register_node("mesewars:mese_orange", {
  description = "Oranges Mese",
  groups = {cracky=1, mw_cracky = 1},
  tiles = {"mesewars_orange_mese.png"},
  drop = "",
  on_dig = function(pos, node, player)
	  local p_name = player:get_player_name()
	  if arena_lib.is_player_in_arena(p_name, "mesewars_2") and not(arena_lib.is_player_spectating(p_name)) then
		  local arena = arena_lib.get_arena_by_player(p_name)
		  if arena.players[p_name].teamID == 1 then
			  minetest.chat_send_all("You can't dig your own mese")
		  else
			  minetest.node_dig(pos, node, player)
			  arena.teams[1].mese = false
			  mesewars.p2.update_mese_HUD(arena, 1)
			  for pl_name, stats in pairs(arena.players) do
				  minetest.chat_send_player(pl_name ,"Oranges Mese was dug by " .. p_name)
			  end
			  mesewars.add_mesecoins(p_name, 20)
			  minetest.chat_send_player(p_name, "You recieved 20 mesecoins")
		  end
	  else
		  minetest.node_dig(pos, node, player)
	  end
  end
})

-- Exchanger

minetest.register_craftitem("mesewars:exchanger",{
  description = "Mesewars Exchanger",
  inventory_image = "exchange.png",
})

minetest.register_craftitem("mesewars:coin",{
  description = "Mesewars Coin",
  inventory_image = "Mesecoin.png",
})
-- Drooper


-- Api
function mesewars.register_dropper(dropper, ingot, def)
minetest.register_node(dropper, {
  description = def.material .. "dropper",
  tiles = def.dropper_tiles,
})

minetest.register_craftitem(ingot, {
  description = def.ingot_description,
  inventory_image = def.ingot_image,
})

minetest.register_abm{
  label = def.material .."dropper",
	nodenames = {dropper},
	interval = def.interval,
	chance = 1,
	action = function(pos)
		minetest.spawn_item({x=pos.x, y=pos.y+1, z=pos.z}, ingot)
	end,
}
end

mesewars.register_dropper("mesewars:brickdropper", "mesewars:brick_ingot",{
  material = "Brick",
  dropper_tiles = {"default_brick.png"},
  ingot_description = "Brickingot",
  ingot_image = "default_clay_brick.png",
  interval = 1,
})


mesewars.register_dropper("mesewars:steeldropper", "mesewars:steel_ingot",{
  material = "Steel",
  dropper_tiles = {"default_steel_block.png"},
  ingot_description = "Steelingot",
  ingot_image = "default_steel_ingot.png",
  interval = 5,
})

mesewars.register_dropper("mesewars:golddropper", "mesewars:gold_ingot",{
  material = "Gold",
  dropper_tiles = {"default_gold_block.png"},
  ingot_description = "Goldingot",
  ingot_image = "default_gold_ingot.png",
  interval = 20,
})

mesewars.register_dropper("mesewars:diamonddropper", "mesewars:diamond",{
  material = "Diamond",
  dropper_tiles = {"default_diamond_block.png"},
  ingot_description = "Diamond",
  ingot_image = "default_diamond.png",
  interval = 60,
})


--Shop
