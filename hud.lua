function mesewars.p2.generate_HUD(arena, p_name)
	local player = minetest.get_player_by_name(p_name)
	local background_
	local orange_
	local blue_
	local mese_existing_text = "yes"
	local pos_x = 0.005
	local pos_y = 0.4

	background_ = player:hud_add({
      hud_elem_type = "image",
      position  = {x = pos_x, y = pos_y},
      text      = "mesewars_hud_background.png",
      scale     = { x = 2, y = 2},
      alignment = { x = 1, y = 1},
    })

	if arena.teams[1].mese == true then
		mese_existing_text = "yes"
	else
		mese_existing_text = "no"
	end

    orange_ = player:hud_add({
      hud_elem_type = "text",
      position  = {x = pos_x+0.005, y = pos_y+0.005},
      text = "Orange: "..mese_existing_text,
      scale     = { x = 50, y = 10},
	  alignment = { x = 1, y = 1},
      number    = 0xffa500,
    })

	if arena.teams[2].mese == true then
		mese_existing_text = "yes"
	else
		mese_existing_text = "no"
	end

    blue_ = player:hud_add({
      hud_elem_type = "text",
      position  = {x = pos_x+0.005, y = pos_y+0.025},
      text = "Blue: "..mese_existing_text,
      scale     = { x = 50, y = 10},
	  alignment = { x = 1, y = 1},
      number    = 0x0000CD,
    })

	mesewars.p2.saved_huds[p_name] = {
		background = background_,
		orange = orange_,
		blue = blue_
    }
end

function mesewars.p2.remove_HUD(arena, p_name)
    local player = minetest.get_player_by_name(p_name)

    if not mesewars.p2.saved_huds[p_name] then return end

    for name, id in pairs(mesewars.p2.saved_huds[p_name]) do
        if type(id) == "table" then id = id.id end
        player:hud_remove(id)
    end

    mesewars.p2.saved_huds[p_name] = {}
end

function mesewars.p2.update_mese_HUD(arena, teamID)
	if teamID == 1 then
		if arena.teams[teamID].mese then
			for p_name in pairs(arena.players_and_spectators) do
				local player = minetest.get_player_by_name(p_name)
				player:hud_change(mesewars.p2.saved_huds[p_name].orange, "text", "Orange: yes")
			end
		else
			for p_name in pairs(arena.players_and_spectators) do
				local player = minetest.get_player_by_name(p_name)
				player:hud_change(mesewars.p2.saved_huds[p_name].orange, "text", "Orange: no")
			end
		end
	elseif teamID == 2 then
		if arena.teams[teamID].mese then
			for p_name in pairs(arena.players_and_spectators) do
				local player = minetest.get_player_by_name(p_name)
				player:hud_change(mesewars.p2.saved_huds[p_name].blue, "text", "Blue: yes")
			end
		else
			for p_name in pairs(arena.players_and_spectators) do
				local player = minetest.get_player_by_name(p_name)
				player:hud_change(mesewars.p2.saved_huds[p_name].blue, "text", "Blue: no")
			end
		end
	end
end

function mesewars.hud_update_to(arena, hud, to)
	for p_name in pairs(arena.players) do
		local player = minetest.get_player_by_name(p_name)
		player:hud_change(hud, "text", to)
	end
end
