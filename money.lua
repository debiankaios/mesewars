storage = minetest.get_mod_storage()

local mesecoins = "mesecoins"

function mesewars.set_mesecoins(player, value)
  storage:set_string(player..':'..mesecoins, value)
end

function mesewars.get_mesecoins(player)
  local mc_value = storage:get_string(player..':'..mesecoins)
  return mc_value
end

function mesewars.sub_mesecoins(player, value)
  local mc_value = mesewars.get_mesecoins(player)
  storage:set_string(player..':'..mesecoins, mc_value - value)
end

function mesewars.add_mesecoins(player, value)
  local mc_value = mesewars.get_mesecoins(player)
  storage:set_string(player..':'..mesecoins, mc_value + value)
end

function mesewars.transfer_mesecoins(from_player,to_player, value)
  mesewars.sub_mesecoins(from_player, value)
  mesewars.add_mesecoins(to_player, value)
end

minetest.register_on_joinplayer(function(player)
  local user = player:get_player_name()
  if mesewars.get_mesecoins(user) == "" then
    mesewars.set_mesecoins(user, 0)
  end
end)


--[[ FIXME minetest.register_privilege("mesecoins_admin", "Needed to do admin-actions which have todo with mesecoins")

ChatCmdBuilder.new("mesecoins", function(cmd)
  -- create arena
  cmd:sub("set :target :value", function(name, target, value)
    if target then
      mesewars.set_mesecoins(target, value)
      return true, "Mesecoins from " .. target .. " set too: " .. value
    else
      return false, "Player " .. target .. " never joined"
    end

  end)

  cmd:sub("get :target", function(name, target)
    if target then
      local value = mesewars.get_mesecoins(target)
      return true, target .. " has " .. value .. " mesecoins"
    else
      return false, "Player " .. target .. " never joined"
    end
  end)

end, {
  description = [[
  (/help mesecoins)

  - set <player> <value> (privs needed: admin)
  - get <player>
  >>,
})]]
