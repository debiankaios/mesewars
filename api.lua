-- Code is from WorldEdit
function mesewars.sort_pos(pos1, pos2)
	pos1 = {x=pos1.x, y=pos1.y, z=pos1.z}
	pos2 = {x=pos2.x, y=pos2.y, z=pos2.z}
	if pos1.x > pos2.x then
		pos2.x, pos1.x = pos1.x, pos2.x
	end
	if pos1.y > pos2.y then
		pos2.y, pos1.y = pos1.y, pos2.y
	end
	if pos1.z > pos2.z then
		pos2.z, pos1.z = pos1.z, pos2.z
	end
	return pos1, pos2
end

function mesewars.keep_loaded(pos1, pos2)
	-- Create a vmanip and read the area from map, this
	-- causes all MapBlocks to be loaded into memory.
	-- This doesn't actually *keep* them loaded, unlike the name implies.
	local manip = minetest.get_voxel_manip()
	manip:read_from_map(pos1, pos2)
end

function mesewars.clear_objects(pos1, pos2)
	pos1, pos2 = mesewars.sort_pos(pos1, pos2)

	mesewars.keep_loaded(pos1, pos2)

	local function should_delete(obj)
		-- Avoid players and WorldEdit entities
		if obj:is_player() then
			return false
		end
		local entity = obj:get_luaentity()
		return not entity or not entity.name:find("^worldedit:")
	end

	-- Offset positions to include full nodes (positions are in the center of nodes)
	local pos1x, pos1y, pos1z = pos1.x - 0.5, pos1.y - 0.5, pos1.z - 0.5
	local pos2x, pos2y, pos2z = pos2.x + 0.5, pos2.y + 0.5, pos2.z + 0.5

	local count = 0
	if minetest.get_objects_in_area then
		local objects = minetest.get_objects_in_area({x=pos1x, y=pos1y, z=pos1z},
			{x=pos2x, y=pos2y, z=pos2z})

		for _, obj in pairs(objects) do
			if should_delete(obj) then
				obj:remove()
				count = count + 1
			end
		end
		return count
	end

	-- Fallback implementation via get_objects_inside_radius
	-- Center of region
	local center = {
		x = pos1x + ((pos2x - pos1x) / 2),
		y = pos1y + ((pos2y - pos1y) / 2),
		z = pos1z + ((pos2z - pos1z) / 2)
	}
	-- Bounding sphere radius
	local radius = math.sqrt(
			(center.x - pos1x) ^ 2 +
			(center.y - pos1y) ^ 2 +
			(center.z - pos1z) ^ 2)
	for _, obj in pairs(minetest.get_objects_inside_radius(center, radius)) do
		if should_delete(obj) then
			local pos = obj:get_pos()
			if pos.x >= pos1x and pos.x <= pos2x and
					pos.y >= pos1y and pos.y <= pos2y and
					pos.z >= pos1z and pos.z <= pos2z then
				-- Inside region
				obj:remove()
				count = count + 1
			end
		end
	end
	return count
end

-- Protection

local old_is_protected = minetest.is_protected

 -- Checking a position local spawn_protected = minetest.is_protected({x=0, y=0, z=0}, playername)

function minetest.is_protected(pos, name)
    if arena_lib.is_player_in_arena(name, "mesewars_2") or arena_lib.is_player_in_arena(name, "mesewars") then
        local arena = arena_lib.get_arena_by_player(name)
        if arena then
            if arena.in_game == false then return false end
            npos1, npos2 = vector.sort(arena.nepoint, arena.swpoint)
            if pos.x >= npos1.x and pos.y >= npos1.y and pos.z >= npos1.z and pos.x <= npos2.x and pos.y <= npos2.y and pos.z <= npos2.z then
                return false
            end
        end
    end
    return old_is_protected(pos, name)
end
