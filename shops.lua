--[[prices = {
  wool = {
    perblock = {""}
    small =
    medium =
    large =
  },
}]]


minetest.register_node("mesewars:shop_blue", {
  description = "Blue Shop", -- Add image later!
  groups = {unbreakable = 1},
  drop = "",
  paramtype2 = "facedir",
  tiles = {"easyvend_vendor_top_blue.png", "easyvend_vendor_top_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_front_on_blue.png"},
  on_rightclick = function(pos, node, player, itemstack, pointed_thing)
    local name = player:get_player_name()
    minetest.show_formspec(name, "mesewars:shop_main",
      "size[6,3]" ..
      "item_image_button[0,1;1,1;mesewars:wool_blue;blocksb;]" ..
      "tooltip[blocksb;Blocks]" ..
      "item_image_button[1,1;1,1;default:sword_steel;swords;]" ..
      "tooltip[swords;Swords]" ..
      "item_image_button[2,1;1,1;3d_armor:chestplate_steel;armor;]" ..
      "tooltip[armor;Armor, not aviable now]" ..
      "item_image_button[3,1;1,1;default:pick_steel;tools;]" ..
      "tooltip[tools;Tools]" ..
      "item_image_button[4,1;1,1;default:chest;special;]" ..
      "tooltip[special;Special]" ..
      "image_button[5,1;1,1;healing_potion.png;potions not aviable now,;]" ..
      "tooltip[potions;Potions, not aviable now]" ..
      --[["item_image_button[6,1;1,1;bows:bow;bows not aviable now,;]"..
      "tooltip[bows;Bows, not aviable now]" ..]]
      "item_image_button[0,2;1,1;farming:bread;food;]"..
      "tooltip[food;Food, not aviable now]"..
      "item_image_button[1,2;1,1;mesewars:exchanger;exchange not aviable now,;]"..
      "tooltip[exchange;Exchange, not aviable now]"..
      "item_image_button[2,2;1,1;mesewars:coin;Exchange to Mesecoins not aviable now,;]"..
      "tooltip[mesecoins;Mesecoins, not aviable now]")
  end
})

minetest.register_node("mesewars:shop_orange", {
  description = "Orange Shop",
  groups = {unbreakable = 1},
  drop = "",
  paramtype2 = "facedir",
  tiles = {"easyvend_vendor_top_orange_2.png", "easyvend_vendor_top_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_front_on_orange_2.png"},
  on_rightclick = function(pos, node, player, itemstack, pointed_thing)
    local name = player:get_player_name()
    minetest.show_formspec(name, "mesewars:shop_main",
      "size[6,3]" ..
			--"scroll_container[0,1;5,5;no;horizontal;0.1]"..
      "item_image_button[0,1;1,1;mesewars:wool_orange;blockso;]" ..
      "tooltip[blockso;Blocks]" ..
			--"scrollbar[1,0.2;0.2,1;vertical;scrbar_inv;0.1]" ..
			--"scroll_container_end[]"..
      "item_image_button[1,1;1,1;default:sword_steel;swords;]" ..
      "tooltip[swords;Swords]" ..
      "item_image_button[2,1;1,1;3d_armor:chestplate_steel;armor;]" ..
      "tooltip[armor;Armor, not aviable now]" ..
      "item_image_button[3,1;1,1;default:pick_steel;tools;]" ..
      "tooltip[tools;Tools]" ..
      "item_image_button[4,1;1,1;default:chest;special;]" ..
      "tooltip[special;Special]" ..
      "image_button[5,1;1,1;healing_potion.png;potions,;]" ..
      --[["item_image_button[6,1;1,1;bows:bow;bows;]"..
      "tooltip[bows;Bows, not aviable now]" ..]]
      "item_image_button[0,2;1,1;farming:bread;food;]"..
      "tooltip[food;Food, not aviable now]"..
      "item_image_button[1,2;1,1;mesewars:exchanger;exchange;]"..
      "tooltip[exchange;Exchange, not aviable now]"..
      "item_image_button[2,2;1,1;mesewars:coin;Exchange to Mesecoins ,not aviable now;]"..
      "tooltip[mesecoins;Mesecoins, not aviable now]")
			--"textlist[3,2;1,1;av;hi,hi2,hi3 ]")

  end
})

function mesewars.shop(player, input, output)
  local name = player:get_player_name()
  local inv = player:get_inventory()
  if not inv:contains_item("main", input) then
    minetest.chat_send_player(name, "You don't have enough resouces.")
  elseif not inv:room_for_item("main", output) then
    minetest.chat_send_player(name, "In your inventory is not enough space.")
  else inv:remove_item("main", input)
    inv:add_item("main", output)
  end
end


minetest.register_on_player_receive_fields(function(player, formname, pressed)
  local name = player:get_player_name()
  if formname == "mesewars:shop_main" then
      if pressed["blocksb"] then
        minetest.show_formspec(name, "mesewars:shop_blocksb",
          "size[9,6]" ..
          "label[1,1;Wool]" ..
          "label[1,2;1 Brick to 3]" ..
          "item_image_button[1,3;1,1;mesewars:wool_blue;swoolb;4:1]" ..
          "item_image_button[1,4;1,1;mesewars:wool_blue;mwoolb;32:8]" ..
          "item_image_button[1,5;1,1;mesewars:wool_blue;lwoolb;96:24]" ..
					"label[3,1;Stone]" ..
					"label[3,2;1 Brick to 1]" ..
					"item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
					"item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
					"item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
					"label[5,1;Wood]" ..
					"label[5,2;1 Brick to 1]" ..
					"item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
					"item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
					"item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
					--[["label[5,1;Glass]" ..
					"label[5,2;3 Brick to 1]" ..
					"item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
					"item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
					"item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
					--[["label[7,1;Steel]" ..
					"label[7,2;3 Steel to 1]" ..
					"item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
					"item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
					"item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
					"label[7,1;Obsidian]" ..
					"label[7,2;2 Diamonds to 1]" ..
					"item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
					"item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
					"item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")
        elseif pressed["blockso"] then
            minetest.show_formspec(name, "mesewars:shop_blockso",
              "size[9,6]" ..
              "label[1,1;Wool]" ..
              "label[1,2;1 Brick to 3]" ..
              "item_image_button[1,3;1,1;mesewars:wool_orange;swoolo;4:1]" ..
              "item_image_button[1,4;1,1;mesewars:wool_orange;mwoolo;32:8]" ..
              "item_image_button[1,5;1,1;mesewars:wool_orange;lwoolo;96:24]" ..
              "label[3,1;Stone]" ..
              "label[3,2;1 Brick to 1]" ..
              "item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
              "item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
              "item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
							"label[5,1;Wood]" ..
              "label[5,2;1 Brick to 1]" ..
              "item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
              "item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
              "item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
              --[["label[5,1;Glass]" ..
              "label[5,2;3 Brick to 1]" ..
              "item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
              "item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
              "item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
              --[["label[7,1;Steel]" ..
              "label[7,2;3 Steel to 1]" ..
              "item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
              "item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
              "item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
              "label[7,1;Obsidian]" ..
              "label[7,2;2 Diamonds to 1]" ..
              "item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
              "item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
              "item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")

					elseif pressed["blockso"] then
		           minetest.show_formspec(name, "mesewars:shop_blockso",
		             "size[9,6]" ..
		             "label[1,1;Picks]" ..
		             "label[1,2;1 Brick to 3]" ..
		             "item_image_button[1,3;1,1;mesewars:wool_orange;swoolo;4:1]" ..
		             "item_image_button[1,4;1,1;mesewars:wool_orange;mwoolo;32:8]" ..
	               "item_image_button[1,5;1,1;mesewars:wool_orange;lwoolo;96:24]" ..
								 "label[3,1;Stone]" ..
	               "label[3,2;1 Brick to 1]" ..
	               "item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
	               "item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
	               "item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
	 							"label[5,1;Wood]" ..
	               "label[5,2;1 Brick to 1]" ..
	               "item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
	               "item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
	               "item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
	               --[["label[5,1;Glass]" ..
	               "label[5,2;3 Brick to 1]" ..
	               "item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
	               "item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
	               "item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
	               --[["label[7,1;Steel]" ..
	               "label[7,2;3 Steel to 1]" ..
	               "item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
	               "item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
	               "item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
	               "label[7,1;Obsidian]" ..
	               "label[7,2;2 Gold to 1]" ..
	               "item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
	               "item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
	               "item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")
			elseif pressed["swords"] then
		     minetest.show_formspec(name, "mesewars:shop_swords",
				   "size[9,4]" ..
		       "label[1,1;Stone Sword]" ..
				   "label[1,2;10 Brick to 1]" ..
		       "item_image_button[1,3;1,1;default:sword_stone;1stonesword;1:10]" ..
           "label[3,1;Steel Sword]" ..
		       "label[3,2;15 Steel to 1]" ..
 	         "item_image_button[3,3;1,1;default:sword_steel;1steelsword;1:15]" ..
				   "label[5,1;Mese Sword]" ..
	       	 "label[5,2;10 Gold to 1]" ..
				   "item_image_button[5,3;1,1;default:sword_mese;1mesesword;1:10]" ..
	       	 "label[7,1;Diamond Sword]" ..
				   "label[7,2;9 Diamonds to 1]" ..
		       "item_image_button[7,3;1,1;default:sword_diamond;1diamondsword;1:9]")
				 elseif pressed["tools"] then
	         minetest.show_formspec(name, "mesewars:shop_tools",
	           "size[9,4]" ..
	           "label[1,1;Stone Pick]" ..
	           "label[1,2;10 Brick to 1]" ..
	           "item_image_button[1,3;1,1;default:pick_stone;1stonepick;1:10]" ..
	           "label[3,1;Steel Pick]" ..
	           "label[3,2;5 Steel to 1]" ..
	           "item_image_button[3,3;1,1;default:pick_steel;1steelpick;1:5]" ..
	           "label[5,1;Diamond Pick]" ..
	           "label[5,2;4 Diamonds to 1]" ..
	           "item_image_button[5,3;1,1;default:pick_diamond;1diamondpick;1:4]" ..
	           "label[7,1;Steel Axe]" ..
	           "label[7,2;1 Diamond to 1]" ..
	           "item_image_button[7,3;1,1;default:axe_steel;1steelaxe;1:1]")

      elseif pressed["special"] then
        minetest.show_formspec(name, "mesewars:shop_special",
          "size[6,4]" ..
          "label[1,1;Chest]" ..
          "label[1,2;2 Gold to 1]" ..
          "item_image_button[1,3;1,1;default:chest;1chest;1:2]" ..
          --"label[3,1;Base Teleporter]" ..
          --"label[3,2;10 Steel to 1]" ..
          --"item_image_button[3,3;1,1;mesewars:baseteleport;1basetp;1:10]" ..
          "label[4,1;Enderpearl]" ..
          "label[4,2;4 Diamond to 1]" ..
          "item_image_button[4,3;1,1;enderpearl:ender_pearl;1pearl;1:4]" )
          --"label[7,1;Bridge Builder]" ..
          --"label[7,2;3 Steel to 1]" ..
          --"item_image_button[7,3;1,1;mesewars:bridge;1bridge;1:3]"..
          --"label[9,1;TNT]" ..
          --"label[9,2;3 Steel to 1]" ..
          --"item_image_button[9,3;1,1;tnt:tnt_burning;1tnt;1:3]")
      elseif pressed["potions"] then
        minetest.show_formspec(name, "mesewars:shop_potion",
          "size[9,4]" ..
          "label[1,1;Regen Potion]" ..
          "label[1,2;1 Gold to 1]" ..
          "item_image_button[1,3;1,1;pep:regen;1regen;1:1]" ..
          "label[3,1;Regen Potion2]" ..
          "label[3,2;2 Gold to 1]" ..
          "item_image_button[3,3;1,1;pep:regen2;1regen2;1:2]" ..
          "label[5,1;Speed Potion]" ..
          "label[5,2;15 Steel to 1]" ..
          "item_image_button[5,3;1,1;pep:speedplus;1speed;1:15]" ..
          "label[7,1;Jump Potion]" ..
          "label[7,2;5 Steel to 1]" ..
          "item_image_button[7,3;1,1;pep:jumpplus;1jump;1:5]")
      elseif pressed["bows"] then
        minetest.show_formspec(name, "mesewars:shop_bows",
          "size[5,4]" ..
          "label[1,1;Bow]" ..
          "label[1,2;1 Gold to 1]" ..
          "item_image_button[1,3;1,1;bow:bow;1bow;1:4]" ..
          "label[3,1;Arrow]" ..
          "label[3,2;25 Brick to 5]" ..
          "item_image_button[3,3;1,1;bow:arrow;5arrow;5:25]")
      elseif pressed["food"] then
        minetest.show_formspec(name, "mesewars:shop_food",
          "size[7,4]" ..
          "label[1,1;Mushroom]" ..
          "label[1,2;10 Brick to 1]" ..
          "item_image_button[1,3;1,1;flowers:mushroom_brown;1mushroom;10:1]" ..
          "label[3,1;Apple]" ..
          "label[3,2;15 Brick to 1]" ..
          "item_image_button[3,3;1,1;default:apple;1apple;15:1]"..
          "label[5,1;Bread]" ..
          "label[5,2;1 Steel to 1]" ..
          "item_image_button[5,3;1,1;farming:bread;1bread;1:1]")
      elseif pressed["exchange"] then
        minetest.show_formspec(name, "mesewars:shop_exchange",
          "size[13,6]" ..
          "label[1,3;Buy]" ..
          "label[1,5;Sell]" ..
          "label[3,1;Steel and Brick]" ..
          "label[3,2;5 Bricks = 1 Steel]" ..
          "item_image_button[3,3;1,1;mesewars:steel_ingot;bs;7:1]" ..
          "item_image_button[3,5;1,1;mesewars:brick_ingot;sb;1:5]" ..
          "label[6,1;Gold and Steel]" ..
          "label[6,2;3 Steel = 1 Gold]" ..
          "item_image_button[6,3;1,1;mesewars:gold_ingot;sg;5:1]" ..
          "item_image_button[6,5;1,1;mesewars:steel_ingot;gs;1:3]" ..
          "label[9,1;Diamond and Gold]" ..
          "label[9,2;3 Gold = Diamond]" ..
          "item_image_button[9,3;1,1;mesewars:diamond;gd;5:1]" ..
          "item_image_button[9,5;1,1;mesewars:gold_ingot;dg;1:3]")
      end
  elseif formname == "mesewars:shop_blocksb" then
    if pressed["swoolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wool_blue 4")
    elseif pressed["mwoolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 8", "mesewars:wool_blue 32")
    elseif pressed["lwoolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 24", "mesewars:wool_blue 96")
		elseif pressed["3stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:stone 1")
    elseif pressed["33stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:stone 33")
    elseif pressed["99stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:stone 99")
		elseif pressed["3wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wood 1")
    elseif pressed["33wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:wood 33")
    elseif pressed["99wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:wood 99")
    elseif pressed["3sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "default:sandstone 1")
    elseif pressed["33sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:sandstone 33")
    elseif pressed["99sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:sandstone 99")
    elseif pressed["1glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "default:glass 1")
    elseif pressed["33glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:glass 11")
    elseif pressed["99glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:glass 33")
    elseif pressed["1steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "default:steelblock 1")
    elseif pressed["11steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 33", "default:steelblock 11")
    elseif pressed["33steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 99", "default:steelblock 33")
    elseif pressed["1obs"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:obsidian 1")
    elseif pressed["11obs"] then
      mesewars.shop(player, "mesewars:diamond 22", "default:obsidian 11")
    elseif pressed["33obs"] then
      mesewars.shop(player, "mesewars:diamond 66", "default:obsidian 33")
    end

  elseif formname == "mesewars:shop_blockso" then
    if pressed["swoolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wool_orange 4")
    elseif pressed["32woolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 8", "mesewars:wool_orange 32")
    elseif pressed["lwoolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 24", "mesewars:wool_orange 96")
    elseif pressed["3stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:stone 1")
    elseif pressed["33stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:stone 33")
    elseif pressed["99stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:stone 99")
		elseif pressed["3wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wood 1")
    elseif pressed["33wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:wood 33")
    elseif pressed["99wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:wood 99")
    elseif pressed["1glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "default:glass 1")
    elseif pressed["33glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:glass 11")
    elseif pressed["99glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:glass 33")
    elseif pressed["1steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "default:steelblock 1")
    elseif pressed["11steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 33", "default:steelblock 11")
    elseif pressed["33steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 99", "default:steelblock 33")
    elseif pressed["1obs"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:obsidian 1")
    elseif pressed["11obs"] then
      mesewars.shop(player, "mesewars:diamond 22", "default:obsidian 11")
    elseif pressed["33obs"] then
      mesewars.shop(player, "mesewars:diamond 66", "default:obsidian 33")
    end

	elseif formname == "mesewars:shop_swords" then
    if pressed["1stonesword"] then
      mesewars.shop(player, "mesewars:brick_ingot 10", "default:sword_stone")
    elseif pressed["1steelsword"] then
      mesewars.shop(player, "mesewars:steel_ingot 15", "default:sword_steel")
    elseif pressed["1mesesword"] then
      mesewars.shop(player, "mesewars:gold_ingot 10", "default:sword_mese")
    elseif pressed["1diamondsword"] then
      mesewars.shop(player, "mesewars:diamond 9", "default:sword_diamond")
    end

  elseif formname == "mesewars:shop_armor" then
    if pressed["1woodh"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:helmet_wood")
    elseif pressed["1woodc"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:chestplate_wood")
    elseif pressed["1woodl"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:leggings_wood")
    elseif pressed["1woodb"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:boots_wood")
    elseif pressed["1cactush"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:helmet_cactus")
    elseif pressed["1cactusc"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:chestplate_cactus")
    elseif pressed["1cactusl"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:leggings_cactus")
    elseif pressed["1cactusb"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:boots_cactus")
    elseif pressed["1steelh"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:helmet_steel")
    elseif pressed["1steelc"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:chestplate_steel")
    elseif pressed["1steell"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:leggings_steel")
    elseif pressed["1steelb"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:boots_steel")
    elseif pressed["1diah"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:helmet_diamond")
    elseif pressed["1diac"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:chestplate_diamond")
    elseif pressed["1dial"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:leggings_diamond")
    elseif pressed["1diab"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:boots_diamond")
    end
  elseif formname == "mesewars:shop_tools" then
    if pressed["1stonepick"] then
      mesewars.shop(player, "mesewars:brick_ingot 10", "default:pick_stone")
    elseif pressed["1stoneaxe"] then
      mesewars.shop(player, "mesewars:brick_ingot 5", "default:axe_stone")
    elseif pressed["1stoneshovel"] then
      mesewars.shop(player, "mesewars:brick_ingot 5", "default:shovel_stone")
    elseif pressed["1steelpick"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "default:pick_steel")
    elseif pressed["1steelaxe"] then
      mesewars.shop(player, "mesewars:diamond 1", "default:axe_steel")
    elseif pressed["1steelshovel"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "default:shovel_steel")
    elseif pressed["1mesepick"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:pick_mese")
    elseif pressed["1meseaxe"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:axe_mese")
    elseif pressed["1meseshovel"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:shovel_mese")
    elseif pressed["1diamondpick"] then
      mesewars.shop(player, "mesewars:diamond 4", "default:pick_diamond")
    elseif pressed["1diamondaxe"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:axe_diamond")
    elseif pressed["1diamondshovel"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:shovel_diamond")
    end
  elseif formname == "mesewars:shop_special" then
    if pressed["1chest"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "default:chest")
    elseif pressed["1basetp"] then
      mesewars.shop(player, "mesewars:steel_ingot 10", "mesewars:baseteleport")
    elseif pressed["1pearl"] then
      mesewars.shop(player, "mesewars:diamond 4", "enderpearl:ender_pearl")
    elseif pressed["1bridge"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "mesewars:bridge")
    elseif pressed["1tnt"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "tnt:tnt_burning")
    end
  elseif formname == "mesewars:shop_potion" then
    if pressed["1regen"] then
      mesewars.shop(player, "mesewars:gold_ingot 1", "pep:regen")
    elseif pressed["1regen2"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "pep:regen2")
    elseif pressed["1speed"] then
      mesewars.shop(player, "mesewars:steel_ingot 15", "pep:speedplus")
    elseif pressed["1jump"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "pep:jumpplus")
    end
  elseif formname == "mesewars:shop_bows" then
    if pressed["1bow"] then
      mesewars.shop(player, "mesewars:gold_ingot 1", "bow:bow")
    elseif pressed["5arrow"] then
      mesewars.shop(player, "mesewars:brick_ingot 25", "bow:arrow 5")
    end
  elseif formname == "mesewars:shop_food" then
    if pressed["1mushroom"] then
      mesewars.shop(player, "mesewars:brick_ingot 10", "flowers:mushroom_brown 1")
    elseif pressed["1apple"] then
      mesewars.shop(player, "mesewars:brick_ingot 15", "default:apple 1")
    elseif pressed["1bread"] then
      mesewars.shop(player, "mesewars:steel_ingot 1", "farming:bread 1")
    end
  end
end)
