local S = minetest.get_translator("mesewars")
mesewars = {}
mesewars.p2 = {} --p2 = players: 2
mesewars.p2.saved_huds = {}

--dofile(minetest.get_modpath("mesewars") .. "/settings.lua")
if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(minetest.get_modpath("mesewars") .. "/chatcmdbuilder.lua")
end

arena_lib.register_minigame("mesewars_2", {
  prefix = "[mw2] ",
  hub_spawn_point = { x = 27, y = 27.5, z = -33 },
  queue_waiting_time = 20,
  show_minimap = true,
  properties = {
    nepoint = {x = 0 , y = 0, z = 0},--x = the most west place of your map, y = the deepest place of your map and z = the place which is most north of your map
    swpoint = {x = 0 , y = 0, z = 0},--x = the most east place of your map, y = the highest place of your map and z = the place which is most south of your map
    schemname = "noname", --no .mts needed
    team_blue = {},
    team_orange = {},
    orange_mese = true, --don't change
    blue_mese = true, --don't change
    orange_respawnpoint = {x = 0, y = 0, z = 0},
    blue_respawnpoint = {x = 0, y = 0, z = 0},
  },
  teams = { S("orange"), S("blue"),},
  teams_color_overlay = { "#ffa500", "blue", },
  team_properties = {
    mese = true
  },
  disabled_damage_types = {"fall"},
})

arena_lib.register_minigame("mesewars", {
  prefix = "[mw] ",
  hub_spawn_point = { x = 27, y = 27.5, z = -33 },
  queue_waiting_time = 20,
  show_minimap = true,
  properties = {
    team_blue = {},
    team_green = {},
    team_orange = {},
    team_yellow = {},
  },
  teams = { S("orange"), S("yellow"), S("green"), S("blue"),},
  teams_color_overlay = { "orange", "yellow", "green", "blue", },
  disabled_damage_types = {"fall"},
  --[[
  hotbar = {
    slots = 1,
    background_image = "tntrun_gui_hotbar.png"
  },
  ]]

})

minetest.register_privilege("mesewars_admin", S("Needed for mesewars"))

dofile(minetest.get_modpath("mesewars") .. "/api.lua")
dofile(minetest.get_modpath("mesewars") .. "/hud.lua")
dofile(minetest.get_modpath("mesewars") .. "/money.lua")
dofile(minetest.get_modpath("mesewars") .. "/auto.lua")
dofile(minetest.get_modpath("mesewars") .. "/items.lua")
dofile(minetest.get_modpath("mesewars") .. "/shops.lua")

ChatCmdBuilder.new("mesewars", function(cmd)
  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "mesewars", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "mesewars", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "mesewars", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "mesewars")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "mesewars", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "mesewars", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "mesewars", arena)
  end)

end, {
  description = [[

    (/help mesewars)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
    privs = {
        mesewars_admin = true
    },
})

ChatCmdBuilder.new("mesewars_2", function(cmd)
  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "mesewars_2", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "mesewars_2", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "mesewars_2", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "mesewars_2")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "mesewars_2", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "mesewars_2", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "mesewars_2", arena)
  end)

end, {
  description = [[

    (/help mesewars_2)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
    privs = {
        mesewars_admin = true
    },
})
